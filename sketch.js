let lastPosition = 0;
let albumFront, albumBack;
let angle = 0;
let once = false;

function preload() {
  albumFront = loadImage('leaving.png');
  albumBack = loadImage('album_back_flip.png');
}

function setup() {
  createCanvas(600, 400, WEBGL);
}

function draw() {
  background(0);

  noStroke();



  if(mouseIsPressed && mouseY>0 && mouseY<400 && mouseX>0 && mouseX<600){
    if(once==false){
      once = true;
      initPos = mouseX;
      angle += lastPosition;
    }
    lastPosition = (mouseX-initPos)/150;
    rotateY(angle+lastPosition);
    angle += 0.007;
  }
  else{
    once = false;
    rotateY(angle+lastPosition);
    angle += 0.007;
  }



  push();
  texture(albumFront);
  box(200, 200, 5);
  pop();

  translate(0, 0 , -3);
  push();
  texture(albumBack);
  plane(200);
  pop();
}
