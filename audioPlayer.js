var songs = ["01 - Hello, Goodbye.mp3","02 - IntheSun.mp3","03 - TalktoMe.mp3",
"04 - TheBad&TheUgly.mp3","05 - TooLate.mp3","06 - MovingOn.mp3","07 - SayItNow.mp3"];

var songTimes = [223,256,202,221,323,252,214];


var currentTime = document.getElementById('timeText');
var totalTime = document.getElementById('timeText2');
var progressBar = document.getElementById('progressBar');
var playButton = document.getElementById('playButton');


var song = new Audio();
var currentSong = 0;

window.onload = loadSong;

function loadSong(){
  song.src = "tracks/" + songs[currentSong];
  showDuration();
}

setInterval(updateSongSlider, 1000);

function updateSongSlider(){
  var c = Math.round(song.currentTime);
  progressBar.value = c;
  var text = document.createTextNode(convertTime(c));
  currentTime.textContent = convertTime(c)+" /";
}


function convertTime(secs){
  var min = Math.floor(secs/60);
  var sec = secs % 60;
  min = (min <10) ? "0" + min : min;
  sec = (sec <10) ? "0" + sec : sec;
  return(min+":"+sec);
}


function showDuration(){
  var d = Math.floor(songTimes[currentSong]);
  progressBar.setAttribute("max", d);
  totalTime.textContent = convertTime(d);
}

function playOrPauseSong(){
  if(song.paused){
    song.play();
    playButton.innerHTML = '<i id ="pauseIcon" class="fa fa-pause" aria-hidden="true"></i>';
  }
  else{
    song.pause();
    playButton.innerHTML = '<i id="playIcon" class="fa fa-play" aria-hidden="true"></i>';
  }
}

function seekSong(){
  song.currentTime = progressBar.value;
  currentTime.textContent = convertTime(song.currentTime)+" /";
}

$(function() {
  $("li").on("click",function() {
    song.pause();
    playButton.innerHTML = '<i id="playIcon" class="fa fa-play" aria-hidden="true"></i>';
    currentSong = this.id;
    loadSong();
    $("#playlist li").removeClass("current-song");
    $("#playlist li:eq("+currentSong+")").addClass("current-song");
    song.play();
    playButton.innerHTML = '<i id ="pauseIcon" class="fa fa-pause" aria-hidden="true"></i>';
  });
});

song.addEventListener("ended", function(){
  song.pause();
  song.currentTime = 0;
  playButton.innerHTML = '<i id="playIcon" class="fa fa-play" aria-hidden="true"></i>';
  if(currentSong==6){
    currentSong=0;
  }
  else{
    currentSong++;
  }
  loadSong();
  $("#playlist li").removeClass("current-song");
  $("#playlist li:eq("+currentSong+")").addClass("current-song");
  if(currentSong==0){
    playButton.innerHTML = '<i id="playIcon" class="fa fa-play" aria-hidden="true"></i>';
  }
  else{
    song.play();
    playButton.innerHTML = '<i id ="pauseIcon" class="fa fa-pause" aria-hidden="true"></i>';
  }

});

document.body.onkeyup = function(e){
    if(e.keyCode == 32){
        playOrPauseSong();
    }
}
